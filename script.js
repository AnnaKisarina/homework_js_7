let data = [
    'hello', 
    'name', 
    'world', 
    'miracle', 
    23, 
    55, 
    '23', 
    'phoenix', 
    null, 
    Symbol(), 
    undefined,
    {}
];

function filterBy(arr, type) {
    return arr.filter((element) => (typeof element !== type.toLowerCase()));
}
console.log(filterBy(data, 'object'));
console.log(filterBy(data, 'string'));
console.log(filterBy(data, 'number'));
console.log(filterBy(data, 'Symbol'));
console.log(filterBy(data, 'undefined'));